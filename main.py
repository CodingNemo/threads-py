import inspect
import random
from threading import Thread
from time import sleep, time

import requests

from proxy_settings import PROXY


def print_time(func):
    def wrapper(*args, **kwargs):
        start = time()
        func(*args, **kwargs)
        print(f"{print_function(func.__name__, args)} {round(time() - start, 3)}s")

    return wrapper


class GenericThread(Thread):
    def __init__(self, func):
        super().__init__()
        self.func = func

    def run(self):
        self.func()


yandex_api_key = "trnsl.1.1.20181031T152610Z.86d3588f6544d772.685f4d4a457da7e7a4cec6f83c1a2d9052b3c10a"
yandex_base_url = "https://translate.yandex.net/api/v1.5/tr.json/translate"


@print_time
def say_hello(language):
    wait()
    payload = {
        "key": yandex_api_key,
        "text": "hello world",
        "lang": language
    }

    response = requests.get(yandex_base_url, payload, proxies=PROXY)
    hello_world_translated = response.json()["text"][0]
    print(print_function("say_hello", (language,)) + " " + hello_world_translated)


@print_time
def main():
    tasks = [create_task(lambda: say_hello(lang)) for lang in ["no", "fr", "en", "az", "ar"]]
    await_gather(*tasks)


def create_task(func):
    thread = GenericThread(func)
    thread.start()
    return thread


def await_gather(*tasks):
    [await(task) for task in tasks]


def await(task):
    return task.join()


def function_from_frame(frame_info):
    _, _, _, arg_values = inspect.getargvalues(frame_info.frame)
    return print_function(frame_info.function, tuple(str(val) for val in arg_values.values()))


def print_function(function_name, function_args_values):
    print_values = str(function_args_values)
    return f"{function_name} {print_values} :"


def wait():
    wait_time = random.randint(1, 3)
    stack = inspect.stack()
    caller_frame_info = stack[1]
    print(f"{function_from_frame(caller_frame_info)} waiting {wait_time}s")
    sleep(wait_time)


if __name__ == "__main__":
    main()
